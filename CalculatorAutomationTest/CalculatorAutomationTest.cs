﻿using TestStack.White;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White.UIItems.Finders;
using System.Diagnostics;
using TestStack.White.Factory;
using TestStack.White.UIItems;

namespace CalculatorAutomationTest
{
    [TestClass]
    public class CalculatorAutomationTest
    {
        private const string ExeSourceFile = @"C:\Users\X\source\repos\CalculatorWPF\CalculatorWPF\bin\Debug\CalculatorWPF.exe";
        private static TestStack.White.Application _application;
        private static TestStack.White.UIItems.WindowItems.Window _mainWindow;

        private void StartUpApplication()
        {
            var psi = new ProcessStartInfo(ExeSourceFile);
            _application = Application.AttachOrLaunch(psi);
            _mainWindow = _application.GetWindow(SearchCriteria.ByText("MainWindow"), InitializeOption.NoCache);
        }

        [TestMethod]
        public void Can_Add_Number_To_First_Operand()
        {
            //Arrange
            StartUpApplication();

            //Act
            Button btn0 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn0"));
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            btn0.Click();
            //Assert
            Assert.AreEqual("0", firstOperandTxtBox.Text);

            _application.Kill();

        }

        [TestMethod]
        public void Can_Add_Number_To_Second_Operand()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            btn2.Click();
            Button btnPlus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnPlus"));
            btnPlus.Click();

            //Act
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            btn2.Click();
            btn2.Click();

            //Assert
            Assert.AreEqual("2", firstOperandTxtBox.Text);
            Assert.AreEqual("+", operatorField.Text);
            Assert.AreEqual("22", secondOperandTxtBox.Text);

            _application.Kill();

        }

        [TestMethod]
        public void Can_Click_Operator()
        {
            //Arrange
            StartUpApplication();
            Button btn1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn1"));
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            btn1.Click();

            //Act
            Button btnPlus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnPlus"));
            btnPlus.Click();

            //Assert
            Assert.AreEqual("1", firstOperandTxtBox.Text);
            Assert.AreEqual("+",operatorField.Text);
            

            _application.Kill();

        }

        [TestMethod]
        public void Can_Clear_Field()
        {
            //Arrange
            StartUpApplication();

            //Act
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            btn5.Click();
            Button btnClear = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnClear"));
            btnClear.Click();
            //Assert
            Assert.AreEqual("", firstOperandTxtBox.Text);

            _application.Kill();

        }

        [TestMethod]
        public void Can_Clear_All()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btnPlus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnPlus"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));

            btn2.Click();
            btnPlus.Click();
            btn2.Click();
            btn2.Click();
            btnEquals.Click();

            //Act
            Button btnClearAll = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnClearAll"));
            btnClearAll.Click();
            //Assert
            Assert.AreEqual("", firstOperandTxtBox.Text);
            Assert.AreEqual("", operatorField.Text);
            Assert.AreEqual("", secondOperandTxtBox.Text);
            Assert.AreEqual("", resultField.Text);
            _application.Kill();

        }

        [TestMethod]
        public void Can_Sum_Two_Numbers()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));

            Button btnPlus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnPlus"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));
            btn5.Click();
            btn2.Click();
            btnPlus.Click();
            btn2.Click();
            btn2.Click();

            //Act
            btnEquals.Click();
            
            //Assert
            Assert.AreEqual("52", firstOperandTxtBox.Text);
            Assert.AreEqual("+", operatorField.Text);
            Assert.AreEqual("22", secondOperandTxtBox.Text);
            Assert.AreEqual("74", resultField.Text);
            _application.Kill();

        }

        [TestMethod]
        public void Can_Subtract_Two_Numbers()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));

            Button btnMinus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnMinus"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));
            btn5.Click();
            btn2.Click();
            btnMinus.Click();
            btn2.Click();
            btn5.Click();

            //Act
            btnEquals.Click();

            //Assert
            Assert.AreEqual("52", firstOperandTxtBox.Text);
            Assert.AreEqual("-", operatorField.Text);
            Assert.AreEqual("25", secondOperandTxtBox.Text);
            Assert.AreEqual("27", resultField.Text);
            _application.Kill();

        }

        [TestMethod]
        public void Can_Multiply_Two_Numbers()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));

            Button btnMultiply = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnMultiply"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));
            btn2.Click();
            btn5.Click();
            btnMultiply.Click();
            btn2.Click();
            btn5.Click();

            //Act
            btnEquals.Click();

            //Assert
            Assert.AreEqual("25", firstOperandTxtBox.Text);
            Assert.AreEqual("*", operatorField.Text);
            Assert.AreEqual("25", secondOperandTxtBox.Text);
            Assert.AreEqual("625", resultField.Text);
            _application.Kill();

        }

        [TestMethod]
        public void Can_Divide_Two_Numbers()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));

            Button btnDivide = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnDivide"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));

            btn5.Click();
            btn2.Click();
            btnDivide.Click();
            btn2.Click();

            //Act
            btnEquals.Click();

            //Assert
            Assert.AreEqual("52", firstOperandTxtBox.Text);
            Assert.AreEqual("/", operatorField.Text);
            Assert.AreEqual("2", secondOperandTxtBox.Text);
            Assert.AreEqual("26", resultField.Text);

            _application.Kill();
        }

        [TestMethod]
        public void Can_Sum_Two_Decimal_Numbers()
        {
            //Arrange
            StartUpApplication();
            TextBox firstOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("firstOperandField"));
            TextBox operatorField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("operatorField"));
            TextBox secondOperandTxtBox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("secondOperandField"));
            TextBox resultField = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("resultField"));
            Button btn2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn2"));
            Button btn5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btn5"));
            Button btnComma = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnComma"));
            Button btnPlus = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnPlus"));
            Button btnEquals = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnEquals"));
            btn5.Click();
            btnComma.Click();
            btn2.Click();
            btnPlus.Click();
            btn2.Click();
            btnComma.Click();
            btn2.Click();

            //Act
            btnEquals.Click();

            //Assert
            Assert.AreEqual("5,2", firstOperandTxtBox.Text);
            Assert.AreEqual("+", operatorField.Text);
            Assert.AreEqual("2,2", secondOperandTxtBox.Text);
            Assert.AreEqual("7,4", resultField.Text);
            _application.Kill();

        }
    }
}
