﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;
using System;

namespace CalculatorTest
{
    [TestClass]
    public class CalculatorTest
    {
        private string firstOperand;
        private string secondOperand;
        private string operatorField;
        private string resultField;

        private void ResetResults()
        {
            firstOperand = string.Empty;
            secondOperand = string.Empty;
            operatorField = string.Empty;
            resultField = string.Empty;
        }
        
        [TestMethod]
        public void TestInitialization()
        {
            ResetResults();
        }

        [TestMethod]
        public void FirstNumberEnteredTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();


            //Act
            _calculator.ReadInput(Number.One);
            _calculator.ReadInput(Number.Two);
            _calculator.ReadInput(Number.Three);

            //Assert
            Assert.AreEqual("123", firstOperand);
        }

        [TestMethod]
        public void SecondNumberEnteredTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.One);
            _calculator.ReadOperation(Operation.Add);

            //Act
            _calculator.ReadInput(Number.One);
            _calculator.ReadInput(Number.Two);

            //Assert
            Assert.AreEqual("1", firstOperand);
            Assert.AreEqual("+", operatorField);
            Assert.AreEqual("12", secondOperand);
        }

        [TestMethod]
        public void OperationClickedTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.One);

            //Act
            _calculator.ReadOperation(Operation.Add);

            //Assert
            Assert.AreEqual("1", firstOperand);
            Assert.AreEqual("+", operatorField);
        }

        [TestMethod]
        public void ClearFirstNumberTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.One);
            _calculator.ReadInput(Number.Two);
            _calculator.ReadInput(Number.Three);

            //Act
            _calculator.ReadOperation(Operation.Clear);

            //Assert
            Assert.AreEqual("", firstOperand);
        }

        [TestMethod]
        public void ClearSecondNumberTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Add);
            _calculator.ReadInput(Number.Seven);
            //Act
            _calculator.ReadOperation(Operation.Clear);

            //Assert
            Assert.AreEqual("9", firstOperand);
            Assert.AreEqual("+", operatorField);
            Assert.AreEqual("", secondOperand);
        }

        [TestMethod]
        public void ClearAllTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Add);
            _calculator.ReadInput(Number.Seven);
            
            //Act
            _calculator.ReadOperation(Operation.ClearAll);

            //Assert
            Assert.AreEqual("", firstOperand);
            Assert.AreEqual("", operatorField);
            Assert.AreEqual("", secondOperand);
        }

        [TestMethod]
        public void AddTwoNumbersTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Add);
            _calculator.ReadInput(Number.Seven);

            //Act
            _calculator.ReadOperation(Operation.Equals);

            //Assert
            Assert.AreEqual("9", firstOperand);
            Assert.AreEqual("+", operatorField);
            Assert.AreEqual("7", secondOperand);
            Assert.AreEqual("16", resultField);
        }

        [TestMethod]
        public void SubtractTwoNumbersTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Subtract);
            _calculator.ReadInput(Number.Seven);

            //Act
            _calculator.ReadOperation(Operation.Equals);

            //Assert
            Assert.AreEqual("9", firstOperand);
            Assert.AreEqual("-", operatorField);
            Assert.AreEqual("7", secondOperand);
            Assert.AreEqual("2", resultField);
        }

        [TestMethod]
        public void MultiplyTwoNumbersTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Multiply);
            _calculator.ReadInput(Number.Seven);

            //Act
            _calculator.ReadOperation(Operation.Equals);

            //Assert
            Assert.AreEqual("9", firstOperand);
            Assert.AreEqual("*", operatorField);
            Assert.AreEqual("7", secondOperand);
            Assert.AreEqual("63", resultField);
        }

        [TestMethod]
        public void DivideTwoNumbersTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadInput(Number.Nine);
            _calculator.ReadOperation(Operation.Divide);
            _calculator.ReadInput(Number.Nine);

            //Act
            _calculator.ReadOperation(Operation.Equals);

            //Assert
            Assert.AreEqual("99", firstOperand);
            Assert.AreEqual("/", operatorField);
            Assert.AreEqual("9", secondOperand);
            Assert.AreEqual("11", resultField);
        }

        [TestMethod]
        public void AddTwoDecimalNumbersTest()
        {
            //Arrange
            var _data = new CalculatorData();
            var _calculator = new CalculatorLogic(_data);
            _calculator.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
            ResetResults();

            _calculator.ReadInput(Number.Nine);
            _calculator.ReadInput(Number.Comma);
            _calculator.ReadInput(Number.Five);
            _calculator.ReadOperation(Operation.Add);
            _calculator.ReadInput(Number.Seven);
            _calculator.ReadInput(Number.Comma);
            _calculator.ReadInput(Number.Four);

            //Act
            _calculator.ReadOperation(Operation.Equals);

            //Assert
            Assert.AreEqual("9,5", firstOperand);
            Assert.AreEqual("+", operatorField);
            Assert.AreEqual("7,4", secondOperand);
            Assert.AreEqual("16,9", resultField);
        }

        private void CalculatorUpdatedHandler(object sender, CalculatorUpdatedEventArgs e)
        {
            firstOperand = e.CalculatorData.First.ValueToString();
            secondOperand = e.CalculatorData.Second.ValueToString();
            operatorField = e.CalculatorData.Operation != Operation.Empty ?
            e.CalculatorData.Operation.ValueToString() : string.Empty;
            resultField = e.HasResult ? e.Result : string.Empty;
        }
    }
}
