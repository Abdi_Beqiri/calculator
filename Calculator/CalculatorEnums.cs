﻿using System.Collections.Generic;

namespace Calculator
{
    public enum InputType
    {
        Number,
        Operation,
        Unknown
    }

    public enum Number
    {
        Comma = -1,
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
    }

    public enum Operation
    {
        Empty = -1,
        Add = 0,
        Subtract = 1,
        Multiply = 2,
        Divide = 3,
        Clear = 4,
        ClearAll = 5,
        Equals = 6,
    }
    public class CalculatorEnums
    {
        public static Dictionary<string, Number> StringToNumbers =
            new Dictionary<string, Number>
        {
            { ",", Number.Comma },
            { "0", Number.Zero },
            { "1", Number.One },
            { "2", Number.Two },
            { "3", Number.Three },
            { "4", Number.Four },
            { "5", Number.Five },
            { "6", Number.Six },
            { "7", Number.Seven },
            { "8", Number.Eight },
            { "9", Number.Nine }            
        };

        public static Dictionary<Number, string> NumbersToString =
            new Dictionary<Number, string>
        {
            { Number.Comma,  ","},
            { Number.Zero, "0" },
            { Number.One, "1" },
            { Number.Two, "2" },
            { Number.Three, "3" },
            { Number.Four, "4" },
            { Number.Five,"5" },
            { Number.Six, "6" },
            { Number.Seven, "7" },
            { Number.Eight,"8" },
            { Number.Nine,"9" },
        };

        public static Dictionary<string, Operation> StringToOperations =
            new Dictionary<string, Operation>
        {
            { "+", Operation.Add },
            { "-", Operation.Subtract },
            { "*", Operation.Multiply },
            { "/", Operation.Divide },
            { "CE", Operation.Clear },
            { "C", Operation.ClearAll },
            { "=", Operation.Equals },
        };

        public static Dictionary<Operation, string> OperationsToString =
            new Dictionary<Operation, string>
        {
            { Operation.Add, "+" },
            { Operation.Subtract, "-" },
            { Operation.Multiply, "*" },
            { Operation.Divide, "/" },
            { Operation.Clear, "CE" },
            { Operation.ClearAll, "C" },
            { Operation.Equals, "=" },
        };
    }
}
