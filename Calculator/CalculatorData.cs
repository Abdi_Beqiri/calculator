﻿using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class CalculatorData
    {
        public List<Number> First = new List<Number>();
        public List<Number> Second = new List<Number>();

        public Operation Operation = Operation.Empty ;

        public void ClearAll()
        {
            Operation = Operation.Empty;
            First = new List<Number>();
            Second = new List<Number>();
        }

        public void Clear()
        {
            if (Operation == Operation.Empty)
                First.Clear();
            else
                Second.Clear();
        }

        public void CommaCheck(Number value)
        {
            if (!First.Contains(Number.Comma))
                First.Add(value);
            else if (!Second.Contains(Number.Comma))
                Second.Add(value);
        }

        public void AppendToOperand(Number value)
        {
            if (Operation == Operation.Empty)
                    First.AddWithCheck(value);
            else
                    Second.AddWithCheck(value);
        }

        
    }
}
