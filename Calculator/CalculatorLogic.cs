﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class CalculatorLogic
    {
        private readonly CalculatorData _data;
        Dictionary<Operation, Func<decimal, decimal, decimal>> _operations =
        new Dictionary<Operation, Func<decimal, decimal, decimal>>
            {
                { Operation.Add, (a, b) => a + b},
                { Operation.Subtract, (a, b) => a - b},
                { Operation.Multiply, (a, b) => a * b},
                { Operation.Divide, (a, b) => a / b},
            };
        public CalculatorLogic(CalculatorData data=null)
        {
            _data = data ?? new CalculatorData();
        }

        public void ReadInput(Number c)
        {
           _data.AppendToOperand(c);
            NotifyUpdated();
        }

        public void ReadOperation(Operation c)
        {
            if (_operations.ContainsKey(c) && _data.First.Any())
                _data.Operation = c;
            else if (c == Operation.Clear)
                _data.Clear();
            else if (c == Operation.ClearAll)
                _data.ClearAll();
            else
            {
                (var hasResult, var result) = Execute();
                NotifyUpdated(hasResult, result);
                return ;
            }
            NotifyUpdated();
        }

        private (bool, decimal) Execute()
        {
            var hasResult = _data.Operation != Operation.Empty 
                && _data.First.Any() 
                && _data.Second.Any();

            decimal result = decimal.MaxValue;

            if (hasResult && _operations.ContainsKey(_data.Operation))
            {
                if (_data.Operation == Operation.Divide
                    && !_data.Second.Any(x => x != Number.Zero))
                    throw new DivideByZeroException("Cannot divide by zero");
                result = _operations[_data.Operation](_data.First.ValueToDecimal(), _data.Second.ValueToDecimal());
            }

            return (hasResult, result);
        }
        
       
        
        public event EventHandler<CalculatorUpdatedEventArgs> CalculatorUpdated;


        private void NotifyUpdated(bool hasResult = false, decimal result = decimal.MaxValue)
        {
            var e = new CalculatorUpdatedEventArgs
            {
                CalculatorData = _data,
                HasResult = hasResult
            };

            if (hasResult)
                e.Result = result.ToString();

            CalculatorUpdated?.Invoke(this, e);
        }        
    }

    public class CalculatorUpdatedEventArgs: EventArgs
    {
        public CalculatorData CalculatorData;
        public bool HasResult;
        public string Result;
    }
}
