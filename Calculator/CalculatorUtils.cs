﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    public static class CalculatorUtils
    {
        public static InputType GetInputType(this string s)
        {
            if (CalculatorEnums.StringToNumbers.ContainsKey(s))
                return InputType.Number;

            if (CalculatorEnums.StringToOperations.ContainsKey(s))
                return InputType.Operation;

            return InputType.Unknown;
        }

        public static Number ToNumbers(this string s)
        {
            if (GetInputType(s) == InputType.Number)
                return CalculatorEnums.StringToNumbers[s];
            else
                throw new ArgumentException("Invalid input! No such number!");
        }

        public static string ValueToString(this Number number)
        {
           return CalculatorEnums.NumbersToString[number];
        }

        public static string ValueToString(this List<Number> numbers)
        {
            var result = string.Empty;
            foreach (var number in numbers)
                result += number.ValueToString();
            return result;
        }

        public static decimal ValueToDecimal(this List<Number> numbers)
        {
            return Convert.ToDecimal(numbers.ValueToString());
        }

        public static Operation ToOperations(this string s)
        {
            if (GetInputType(s) == InputType.Operation)
                return CalculatorEnums.StringToOperations[s];
            else
                throw new ArgumentException("Invalid input! No such operation!");
        }

        public static string ValueToString(this Operation op)
        {
            if (op == Operation.Empty)
                return "";
            return CalculatorEnums.OperationsToString[op];
        }

        public static void AddWithCheck(this List<Number> list, Number value)
        {
            if (value == Number.Comma)
                if (list.Contains(value))
                    return;
                else if (list.Count == 0)
                    list.Add(Number.Zero);

            list.Add(value);
        }
    }
}
