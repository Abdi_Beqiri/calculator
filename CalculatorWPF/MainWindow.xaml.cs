﻿using Calculator;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CalculatorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml 
    /// </summary>
    public partial class MainWindow : Window
    {
        private CalculatorLogic _calculatorLogic = new CalculatorLogic();
        private Number[] _numbers = new Number[10] { Number.One,  Number.Two, Number.Three, Number.Four, Number.Five,
                                                     Number.Six, Number.Seven, Number.Eight, Number.Nine, Number.Comma };
        private Operation[] operations = new Operation[] {Operation.Add, Operation.Subtract, Operation.Multiply,
                                                          Operation.Divide, Operation.Clear, Operation.ClearAll, Operation.Equals };
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var numberButton in new Button[] { btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9 })
                numberButton.Click += new RoutedEventHandler(ClickHandler);

            foreach (var operatorButton in new Button[] { btnOperatorPlus, btnOperatorMinus, btnOperatorDivide, btnOperatorMultiply, btnEquals })
                operatorButton.Click += new RoutedEventHandler(ClickHandler);

            _calculatorLogic.CalculatorUpdated += new EventHandler<CalculatorUpdatedEventArgs>(CalculatorUpdatedHandler);
        }

        private void CalculatorUpdatedHandler(object sender, CalculatorUpdatedEventArgs e)
        {
            firstOperand.Text = e.CalculatorData.First.ValueToString();
            secondOperand.Text = e.CalculatorData.Second.ValueToString();
            operatorField.Text = e.CalculatorData.Operation != Operation.Empty ? 
            e.CalculatorData.Operation.ValueToString() : string.Empty;
            resultField.Text = e.HasResult ? e.Result: string.Empty;
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            var value = ((Button)sender).Content.ToString();
            var inputType = value.GetInputType();
            try
            {
                if (inputType == InputType.Number)
                    _calculatorLogic.ReadInput(value.ToNumbers());
                else if (inputType == InputType.Operation)
                    _calculatorLogic.ReadOperation(value.ToOperations());
            }catch(DivideByZeroException ex)
            {
                MessageBox.Show("Cannot divide by zero!");
            }
        }   
    }
}
